<?php

namespace Bim\Util;

use Bitrix\Main,
    ConsoleKit\Colors;

class Base
{
    /** @var \BaseCommand */
    protected static $command = null;

    /**
     * Подключит модули
     *
     * @param $arModulesList
     *
     * @throws Main\LoaderException
     */
    protected static function includeModule($arModulesList)
    {
        if (!\is_array($arModulesList)) {
            $arModulesList = [$arModulesList];
        }

        foreach ($arModulesList as $module) {
            if (!Main\Loader::includeModule($module)) {
                throw new Main\LoaderException("Не установлен модуль {$module}");
            }
        }
    }

    /**
     * Проверит существование Типа ИБ
     *
     * @param $id
     *
     * @return bool
     * @throws Main\LoaderException
     */
    protected static function iBlockTypeExists($id): bool
    {
        $res = self::getIblockType($id);
        if (!empty($res)) {
            self::printInfo("Тип ИБ {$id} уже существует");

            return true;
        }

        return false;
    }

    /**
     * Вернет список Типов ИБ
     *
     * @param $id
     *
     * @return array
     * @throws Main\LoaderException
     */
    protected static function getIblockType($id): array
    {
        self::includeModule(['iblock']);

        $dbIblockType = \CIBlockType::GetList(
            [],
            ['ID' => $id]
        );
        $res = [];
        while ($row = $dbIblockType->Fetch()) {
            $res[] = $row;
        }

        return $res;
    }

    /**
     * Проверит существование ИБ
     *
     * @param $code
     *
     * @return bool
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     */
    protected static function iBlockExists($code): bool
    {
        $iblock = self::getIblock($code);

        if (!empty($iblock['ID'])) {
            self::printInfo("ИБ с кодом {$code} уже существует");

            return true;
        }

        return false;
    }

    /**
     * Проверит сущестование св-ва
     *
     * @param $propertyCode
     * @param $iBlockCode
     *
     * @return bool
     * @throws Main\LoaderException
     */
    protected static function propertyExists($propertyCode, $iBlockCode): bool
    {
        $arProperty = self::getPropertyList($propertyCode, $iBlockCode);
        if (!empty($arProperty)) {
            self::printInfo("Свойство {$propertyCode} в ИБ {$iBlockCode} уже существует");

            return true;
        }

        return false;
    }

    /**
     * Вернет список св-в
     *
     * @param $propertyCode
     * @param $iBlockCode
     *
     * @return array
     * @throws Main\LoaderException
     */
    protected static function getPropertyList($propertyCode, $iBlockCode): array
    {
        self::includeModule(['iblock']);

        $result = [];
        $obj = \CIBlockProperty::GetList(
            [],
            ['CODE' => $propertyCode, 'IBLOCK_CODE' => $iBlockCode]
        );
        while ($row = $obj->Fetch()) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Вернет информацию о ИБ
     *
     * @param $code
     *
     * @return array|false
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     */
    protected static function getIblock($code)
    {
        self::includeModule(['iblock']);

        $iblockIterator = \Bitrix\Iblock\IblockTable::query()
            ->setFilter(['CODE' => $code])
            ->setSelect(['ID'])
            ->setLimit(1)
            ->exec();
        return $iblockIterator->fetch();
    }

    /**
     * Проверит существование элемента по его Символьному коду
     *
     * @param $iblockId
     * @param $code
     *
     * @return bool
     * @throws Main\LoaderException
     */
    protected static function elementExistsByCode($iblockId, $code): bool
    {
        $result = self::getElements(
            [
                'IBLOCK_ID' => $iblockId,
                'CODE' => $code,
            ],
            ['ID'],
            1
        );

        if (!empty($result)) {
            return true;
        }

        return false;
    }

    /**
     * Получить списов элементов
     *
     * @param       $arFilter
     * @param array $arSelect
     * @param int   $count
     *
     * @return array
     * @throws Main\LoaderException
     */
    protected static function getElements($arFilter, $arSelect = ['*'], $count = 0): array
    {
        self::includeModule(['iblock']);

        $result = [];

        if (empty($arFilter)) {
            return $result;
        }

        $obRes = \CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            ((int)$count > 0 ? ['nTopCount' => (int)$count] : false),
            $arSelect
        );

        while ($row = $obRes->Fetch()) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Обновить элемент ИБ
     *
     * @param array $arFields
     * @param array $arProperty
     * @param int   $elementId
     * @param int   $iblockId
     *
     * @return bool
     */
    public static function updateElement(array $arFields, array $arProperty, int $elementId, int $iblockId): bool
    {
        if (!empty($arFields)) {
            $objElement = new \CIBlockElement();
            $bResultUpdate = $objElement->Update($elementId, $arFields, false, false);

            if ($bResultUpdate === false) {
                self::printError($objElement->LAST_ERROR, true);
                return false;
            }
        }

        if (!empty($arProperty)) {
            \CIBlockElement::SetPropertyValuesEx(
                $elementId,
                $iblockId,
                $arProperty
            );
        }


        self::printSuccess("Элемент {$elementId} в ИБ {$iblockId} обновлен");

        return true;
    }

    /**
     * Создание элемента ИБ
     *
     * @param array $arParams
     *
     * @return bool
     * @throws Main\LoaderException
     */
    protected static function addElement(array $arParams): bool
    {
        self::includeModule(['iblock']);

        $objElement = new \CIBlockElement;
        $elementId = $objElement->Add(
            $arParams,
            false,
            false
        );

        if ($elementId === false) {
            self::printError("Элемент {$arParams['NAME']} [{$arParams['CODE']}] в ИБ {$arParams['IBLOCK_ID']} НЕ создан: " . $objElement->LAST_ERROR, true);
            return false;
        }

        self::printSuccess("Создан элемент {$arParams['NAME']} [{$arParams['CODE']}] в ИБ {$arParams['IBLOCK_ID']}");
        return true;
    }

    /**
     * Создать сво-во
     *
     * @param array $arParams
     *
     * @return bool
     * @throws Main\LoaderException
     * @throws \Exception
     */
    protected static function addProperty(array $arParams): bool
    {
        if (empty($arParams['CODE']) || empty($arParams['IBLOCK_CODE'])) {
            self::printError(
                "Неверные параметры при создании св-ва {$arParams['CODE']} в {$arParams['IBLOCK_CODE']}",
                true
            );
        }

        if (self::propertyExists($arParams['CODE'], $arParams['IBLOCK_CODE']) === true) {
            return true;
        }

        $arParams['TIMESTAMP_X'] = self::getCurrentDate();

        $result = \Bim\Db\Iblock\IblockPropertyIntegrate::Add($arParams);
        $result = (int)$result;

        if ($result > 0) {
            self::printSuccess("Создано св-ва {$arParams['CODE']} в ИБ {$arParams['IBLOCK_CODE']}");
        } else {
            self::printError("Создать св-ва {$arParams['CODE']} в ИБ {$arParams['IBLOCK_CODE']} НЕ удалось", true);
        }

        return true;
    }

    /**
     * Удалить св-во
     *
     * @param string $propertyCode
     * @param string $iBlockCode
     *
     * @return bool
     * @throws Main\LoaderException
     */
    protected static function removeProperty(string $propertyCode, string $iBlockCode): bool
    {
        self::includeModule(['iblock']);
        $arProperty = self::getPropertyList($propertyCode, $iBlockCode);

        if (empty($arProperty)) {
            self::printInfo("Свойство {$propertyCode} в ИБ {$iBlockCode} не найдено");
            return true;
        }

        $arProperty = reset($arProperty);
        $result = \CIBlockProperty::Delete($arProperty['ID']);

        if ($result === false) {
            self::printError("Св-во {$propertyCode} из ИБ {$iBlockCode} удалить НЕ удалось", true);
            return false;
        }

        self::printSuccess("Удалено св-во {$propertyCode} из ИБ {$iBlockCode}");

        return true;
    }

    /**
     * Создать ИБ
     *
     * @param array $arParams
     *
     * @return bool
     * @throws Exception
     */
    protected static function addIBlock(array $arParams): bool
    {
        if (empty($arParams['CODE'])) {
            self::printError(
                "Неверные параметры при создании ИБ {$arParams['NAME']}. Не указан CODE.",
                true
            );
        }

        if (self::iBlockExists($arParams['CODE']) === true) {
            return true;
        }

        $arParams['TIMESTAMP_X'] = self::getCurrentDate();

        $result = Bim\Db\Iblock\IblockIntegrate::Add($arParams);
        $result = (int)$result;

        if ($result > 0) {
            self::printSuccess("Создан ИБ {$arParams['NAME']} [{$arParams['CODE']}]");
        } else {
            self::printError("Создано св-ва {$arParams['CODE']} в ИБ {$arParams['IBLOCK_CODE']} НЕ удалось", true);
        }

        return true;
    }

    /**
     * Код записи будет формироваться из названия в нижнем регистре в md5
     *
     * @param $name
     *
     * @return string
     */
    protected static function getCodeFromName($name): string
    {
        return md5(mb_strtolower(self::removeExtraSpaces($name)));
    }

    /**
     * Удалит лишние пробелы
     *
     * @param $string
     *
     * @return string
     */
    protected static function removeExtraSpaces($string): string
    {
        return trim(preg_replace('/\s+/', ' ', $string));
    }

    /**
     * Создаст почтовое событие
     *
     * @param array $arParams :
     * пример
     * [
     *     'LID' => 'ru',
     *     'SORT' => 150,
     *     'EVENT_NAME' => 'ABOUT_REQUEST',
     *     'NAME' => 'О заявке',
     *     'DESCRIPTION' => '#REQUEST_ID# - ID заявки
     *     #USER_EMAIL# - контактный емейл пользователя'
     * ]
     *
     * @return bool
     */
    protected static function addEventType(array $arParams): bool
    {
        if (empty($arParams['EVENT_NAME'])) {
            self::printError(
                "Неверные параметры при создании почтового события {$arParams['NAME']}. Не указан EVENT_NAME.",
                true
            );
        }

        $lid = $arParams['LID'] ?? 'ru';

        if (self::eventTypeExists($arParams['EVENT_NAME'], $lid) === true) {
            return true;
        }

        $resultId = \CEventType::Add($arParams);
        $resultId = (int)$resultId;

        if ($resultId > 0) {
            self::printSuccess("Создано почтовое событие {$arParams['EVENT_NAME']}");
        } else {
            self::printError("Почтовое событие {$arParams['EVENT_NAME']} НЕ создано.", true);
        }

        return ($resultId > 0);
    }

    /**
     * Создаст почтовый шаблон
     *
     * @param array $arParams :
     * пример
     * [
     *     'ACTIVE' => 'Y',
     *     'EVENT_NAME' => 'ABOUT_REQUEST',
     *     'LID' => 's1',
     *     'LANGUAGE_ID' => 'ru',
     *     'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
     *     'EMAIL_TO' => '#USER_EMAIL#',
     *     'SUBJECT' => '#SITE_NAME#: Напоминание о заявке №#REQUEST_ID#',
     *     'BODY_TYPE' => 'text',
     *     'SITE_TEMPLATE_ID' => 'main_mail_template',
     *     'MESSAGE' => 'Текст сообщения'
     * ]
     *
     * @return bool
     */
    protected static function addEventMessage(array $arParams): bool
    {
        if (self::eventMessageExists($arParams) === true) {
            return true;
        }

        $eventMessage = new \CEventMessage;
        $resultId = $eventMessage->Add($arParams);
        $resultId = (int)$resultId;

        if ($resultId > 0) {
            self::printSuccess("Создан почтовый шаблон для почтового события {$arParams['EVENT_NAME']}");
        } else {
            self::printError("Почтовый шаблон для почтового события {$arParams['EVENT_NAME']} создать НЕ удалось", true);
        }

        return ($resultId > 0);
    }

    /**
     * Проверит на существование почтового шаблона
     * За уникальност берутся 3 поля:
     * - EVENT_NAME
     * - EMAIL_FROM
     * - EMAIL_TO
     *
     * @param array $arParams
     *
     * @return bool
     */
    protected static function eventMessageExists(array $arParams): bool
    {
        $arEventMessageList = self::getEventMessageList([
            'EVENT_NAME' => $arParams['EVENT_NAME'],
            'EMAIL_FROM' => $arParams['EMAIL_FROM'],
            'EMAIL_TO' => $arParams['EMAIL_TO']
        ]);

        if (empty($arEventMessageList)) {
            return false;
        }

        self::printInfo("Почтовый шаблон from: {$arParams['EMAIL_FROM']}, to: {$arParams['EMAIL_TO']} для почтового события {$arParams['EVENT_NAME']} существует");
        return true;
    }

    /**
     * Обновит почтовый шаблон по его ID
     *
     * @param int   $eventMessageId
     * @param array $arParams
     *
     * @return bool
     */
    protected static function updateEventMessage(int $eventMessageId, array $arParams): bool
    {
        $eventMessage = new \CEventMessage;
        $bResult = $eventMessage->Update($eventMessageId, $arParams);

        if ($bResult === true) {
            self::printSuccess("Обновлен почтовый шаблон {$eventMessageId}");
        } else {
            self::printError("Почтовый шаблон {$eventMessageId} обновить НЕ удалось.", true);
        }

        return $bResult;
    }

    /**
     * Обновит почтовый шаблон по КОДУ типа почтового события
     *
     * @param string $eventMessageTypeId
     * @param array  $arParams
     */
    protected static function updateEventMessageByTypeId(string $eventMessageTypeId, array $arParams)
    {
        if (empty($eventMessageTypeId)) {
            self::printError('Необходимо указать тип почтового события', true);
        }

        $arEventMessage = self::getEventMessageList(['TYPE_ID' => $eventMessageTypeId]);
        foreach ($arEventMessage as $arItem) {
            self::updateEventMessage($arItem['ID'], $arParams);
        }
    }

    /**
     * Вернет почтовые шаблоны
     *
     * @param array $arFilter
     *
     * @return array
     */
    protected static function getEventMessageList(array $arFilter): array
    {
        $rsEventMessage = \CEventMessage::GetList(
            $by = 'site_id',
            $order = 'desc',
            $arFilter
        );

        $arResult = [];
        while ($arEventMessage = $rsEventMessage->GetNext()) {
            $arResult[] = $arEventMessage;
        }

        return $arResult;
    }

    /**
     * Проверит существование почтового события
     *
     * @param string $typeId
     * @param string $lid
     *
     * @return bool
     */
    protected static function eventTypeExists(string $typeId, string $lid = 'ru'): bool
    {
        $rsEventType = \CEventType::GetByID($typeId, $lid);
        $arEventType = $rsEventType->Fetch();

        $result = !empty($arEventType);

        if ($result === true) {
            self::printInfo("Тип почтового события {$typeId} уже существует");
        }

        return $result;
    }

    /**
     * Создаст группу пользователей
     *
     * @param array $arParams
     *
     * @return bool
     * @throws Main\ArgumentException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     * @throws Exception
     */
    protected static function addUserGroup(array $arParams): bool
    {
        if (empty($arParams['STRING_ID'])) {
            self::printError(
                "Неверные параметры при создании группы пользователей {$arParams['NAME']}. Не указан STRING_ID.",
                true
            );
        }

        if (self::userGroupExists($arParams['STRING_ID']) === true) {
            return true;
        }

        $result = Bim\Db\Main\GroupIntegrate::Add($arParams);
        $result = ((int)$result > 0);

        if ($result === true) {
            self::printSuccess("Группа пользователей {$arParams['STRING_ID']} создана. НЕ ЗАБУДЬТЕ НАСТРОИТЬ ПРАВА.");
        } else {
            self::printError("Группа пользователей {$arParams['STRING_ID']} НЕ создана.", true);
        }

        return $result;
    }

    /**
     * Проверит существует ли группа пользователей по КОДУ
     *
     * @param string $stringId
     *
     * @return bool
     * @throws Main\ArgumentException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     */
    protected static function userGroupExists(string $stringId): bool
    {
        $group = self::getUserGroup([
            'filter' => ['STRING_ID' => $stringId],
            'limit' => 1
        ]);

        if (empty($group)) {
            return false;
        }

        self::printInfo("Группа пользователей {$stringId} существует.");
        return true;
    }

    /**
     * Вернет существующие группы пользователей
     *
     * @param array $arFilter
     *
     * @return array
     * @throws Main\ArgumentException
     * @throws Main\ObjectPropertyException
     * @throws Main\SystemException
     */
    protected static function getUserGroup(array $arFilter = []): array
    {
        return \Bitrix\Main\GroupTable::getList($arFilter)->fetchAll();
    }

    /**
     * Вернет текущую дату в формате
     *
     * @param string $format
     *
     * @return string
     */
    private static function getCurrentDate(string $format = 'Y-m-d H:i:s'): string
    {
        return date($format);
    }

    /**
     * info
     * @param string $text
     */
    public static function printInfo(string $text)
    {
        self::$command->writeln(self::prepareConsoleText($text), Colors::YELLOW);
    }

    /**
     * error
     *
     * @param string $text
     * @param bool   $die
     */
    public static function printError(string $text, bool $die = false)
    {
        self::$command->writeln(self::prepareConsoleText($text), Colors::RED);

        if ($die === true) {
            die();
        }
    }

    /**
     * success
     * @param string $text
     */
    public static function printSuccess(string $text)
    {
        self::$command->writeln(self::prepareConsoleText($text), Colors::GREEN);
    }

    /**
     * padding
     * @param string $text
     * @throws \ConsoleKit\ConsoleException
     */
    public static function printPadding(string $text)
    {
        $box = new \ConsoleKit\Widgets\Box(self::$command, $text, '');
        $box->write();
        self::$command->writeln('');
    }

    private static function prepareConsoleText(string $test): string
    {
        return "        > {$test}";
    }
}